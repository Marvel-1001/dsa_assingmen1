import ballerina/io;
import ballerina/grpc;
import ballerina/protobuf;

CMSClient ep = check new ("http://localhost:9090");

Course_Assignment DSA_Ass1 = {Assignment_Id: 1001, Assignment_Name: "DSA Assignment 1", hasBeenMarked: false, Mark: 0.0, Assignment_weight: 50, User_Code: "Stdnt123"};
Course_Assignment DSA_Ass2 = {Assignment_Id: 1001, Assignment_Name: "DSA Assignment 2", hasBeenMarked: false, Mark: 0.0, Assignment_weight: 50, User_Code: "Stdnt124"};

Course_Assignment AIR_Ass1 = {Assignment_Id: 2001, Assignment_Name: "AIR Assignment 1", hasBeenMarked: false, Mark: 0.0, Assignment_weight: 50, User_Code: "Stdnt123"};
Course_Assignment AIR_Ass2 = {Assignment_Id: 2001, Assignment_Name: "AIR Assignment 2", hasBeenMarked: false, Mark: 0.0, Assignment_weight: 50, User_Code: "Stdnt124"};

Course_Assignment MIT_Ass1 = {Assignment_Id: 3001, Assignment_Name: "MIT Assignment 1", hasBeenMarked: false, Mark: 0.0, Assignment_weight: 50, User_Code: "Stdnt123"};
Course_Assignment MIT_Ass2 = {Assignment_Id: 3001, Assignment_Name: "MIT Assignment 2", hasBeenMarked: false, Mark: 0.0, Assignment_weight: 50, User_Code: "Stdnt123"};

Course_Assignment OOP_Ass1 = {Assignment_Id: 4001, Assignment_Name: "OOP Assignment 1", hasBeenMarked: false, Mark: 0.0, Assignment_weight: 50, User_Code: "Stdnt124"};
Course_Assignment OOP_Ass2 = {Assignment_Id: 4001, Assignment_Name: "OOP Assignment 2", hasBeenMarked: false, Mark: 0.0, Assignment_weight: 50, User_Code: "Stdnt125"};

Course_Assignment PRG_Ass1 = {Assignment_Id: 5001, Assignment_Name: "PRG Assignment 1", hasBeenMarked: false, Mark: 0.0, Assignment_weight: 50, User_Code: "Stdnt125"};
Course_Assignment PRG_Ass2 = {Assignment_Id: 5001, Assignment_Name: "PRG Assignment 2", hasBeenMarked: false, Mark: 0.0, Assignment_weight: 50, User_Code: "Stdnt125"};

Course_Assignment[] SubmittedAssignments = [DSA_Ass1, DSA_Ass2, AIR_Ass1, AIR_Ass2, MIT_Ass1, MIT_Ass2, OOP_Ass1, OOP_Ass2, PRG_Ass1, PRG_Ass2];

// 5x Courses

Course DSA = {Course_Id: 1001, Course_Name: "DSA", List_Of_Assesments: []};
Course AIR = {Course_Id: 2001, Course_Name: "AIR", List_Of_Assesments: []};
Course MIT = {Course_Id: 3001, Course_Name: "MIT", List_Of_Assesments: []};
Course OOP = {Course_Id: 4001, Course_Name: "OOP", List_Of_Assesments: []};
Course PRG = {Course_Id: 5001, Course_Name: "PRG", List_Of_Assesments: []};

Course[] Courses = [DSA, AIR, MIT, OOP, PRG];

User Admin_usersRequest = {User_Index: 1, User_Id: "1001", User_Type: "Admin", User_Code: "Admin123", User_Name: "Super Admin"};

User Assessor_usersRequest = {User_Index: 2, User_Id: "1002", User_Type: "Assessor", User_Code: "Ass123", User_Name: "Kapuire"};
User Assessor_usersRequest2 = {User_Index: 3, User_Id: "1003", User_Type: "Assessor", User_Code: "Ass124", User_Name: "Jose"};

User Mikeal_usersRequest = {User_Index: 4, User_Id: "1004", User_Type: "Student", User_Code: "Stdnt123", User_Name: "Mikeal"};
User Raven_usersRequest = {User_Index: 5, User_Id: "1005", User_Type: "Student", User_Code: "Stdnt124", User_Name: "Raven"};
User Ndeshi_usersRequest = {User_Index: 6, User_Id: "1006", User_Type: "Student", User_Code: "Stdnt125", User_Name: "Ndeshi"};

User[] users = [Admin_usersRequest, Assessor_usersRequest, Assessor_usersRequest2, Mikeal_usersRequest, Raven_usersRequest, Ndeshi_usersRequest];

public function main() returns error? {

    CreateUsers(); //Create Users

    CreateCourses(); //Create courses

    AssignCourses(); //Assign courses

    SubmitAssignments(); //Submit Assignments

    RequestAssignments(); //Request Assignments

    submit_marks(); //Submit courses

    // register(); //Submit courses

    // result request_assignmentsRequest = {code: "ballerina", message: "ballerina"};
    // stream<Course_Assignment, error?> request_assignmentsResponse = check ep->request_assignments(request_assignmentsRequest);
    // check request_assignmentsResponse.forEach(function(Course_Assignment value) {
    //     io:println(value);
    // });

    // Course assign_coursesRequest = {Course_Id: 1, Course_Name: "ballerina", List_Of_Assesments: [{Assignment_Id: 1, Assignment_Name: "ballerina", hasBeenMarked: true, Mark: 1, Assignment_weight: 1}], Assessor_Code: 1};
    // Assign_coursesStreamingClient assign_coursesStreamingClient = check ep->assign_courses();
    // check assign_coursesStreamingClient->sendCourse(assign_coursesRequest);
    // check assign_coursesStreamingClient->complete();
    // result? assign_coursesResponse = check assign_coursesStreamingClient->receiveResult();
    // io:println(assign_coursesResponse);

    // User create_usersRequest = {User_Index: 1, User_Id: "ballerina", User_Type: "ballerina", User_Code: "ballerina", User_Name: "ballerina"};
    // Create_usersStreamingClient create_usersStreamingClient = check ep->create_users();
    // check create_usersStreamingClient->sendUser(create_usersRequest);
    // check create_usersStreamingClient->complete();
    // result? create_usersResponse = check create_usersStreamingClient->receiveResult();
    // io:println(create_usersResponse);

    // Course_Assignment submit_assignmentsRequest = {Assignment_Id: 1, Assignment_Name: "ballerina", hasBeenMarked: true, Mark: 1, Assignment_weight: 1};
    // Submit_assignmentsStreamingClient submit_assignmentsStreamingClient = check ep->submit_assignments();
    // check submit_assignmentsStreamingClient->sendCourse_Assignment(submit_assignmentsRequest);
    // check submit_assignmentsStreamingClient->complete();
    // result? submit_assignmentsResponse = check submit_assignmentsStreamingClient->receiveResult();
    // io:println(submit_assignmentsResponse);

    // Course_Assignment submit_marksRequest = {Assignment_Id: 1, Assignment_Name: "ballerina", hasBeenMarked: true, Mark: 1, Assignment_weight: 1};
    // Submit_marksStreamingClient submit_marksStreamingClient = check ep->submit_marks();
    // check submit_marksStreamingClient->sendCourse_Assignment(submit_marksRequest);
    // check submit_marksStreamingClient->complete();
    // result? submit_marksResponse = check submit_marksStreamingClient->receiveResult();
    // io:println(submit_marksResponse);

    // Course_Assignment registerRequest = {Assignment_Id: 1, Assignment_Name: "ballerina", hasBeenMarked: true, Mark: 1, Assignment_weight: 1};
    // RegisterStreamingClient registerStreamingClient = check ep->register();
    // check registerStreamingClient->sendCourse_Assignment(registerRequest);
    // check registerStreamingClient->complete();
    // result? registerResponse = check registerStreamingClient->receiveResult();
    // io:println(registerResponse);

    // Course create_coursesRequest = {Course_Id: 1, Course_Name: "ballerina", List_Of_Assesments: [{Assignment_Id: 1, Assignment_Name: "ballerina", hasBeenMarked: true, Mark: 1, Assignment_weight: 1}], Assessor_Code: 1};
    // Create_coursesStreamingClient create_coursesStreamingClient = check ep->create_courses();
    // check create_coursesStreamingClient->sendCourse(create_coursesRequest);
    // check create_coursesStreamingClient->complete();
    // result? create_coursesResponse = check create_coursesStreamingClient->receiveResult();
    // io:println(create_coursesResponse);
}

//For the love of functions 

// Public Functions

// where an administrator creates several courses, defines the number of assignments for each course and
// sets the weight for each assignment. This operation returns the code for each created course. It is bidirectional
// streaming;

function CreateCourses() {

    Create_coursesStreamingClient|grpc:Error create_coursesStreamingClient = ep->create_courses();
    grpc:Error? x;
    grpc:Error? y;

    if (create_coursesStreamingClient is error) {
    } else {

        foreach Course c in Courses {
            x = create_coursesStreamingClient->sendCourse(c);
        }

        y = create_coursesStreamingClient->complete();

        result|grpc:Error? create_coursesResponse = create_coursesStreamingClient->receiveResult();

        if (create_coursesResponse is result) {
            io:println(create_coursesResponse.message);
            io:println();
            io:println();
            io:println();
        }
        if (x is error) {
            io:println(x.cause());
        }
        if (y is error) {
            io:println(y.cause());
        }

    }
}

// where several users, each with a specific profile, are created. The users are streamed to the server, and the
// response is returned once the operation completes;
function CreateUsers() {

    Create_usersStreamingClient|grpc:Error create_usersStreamingClient = ep->create_users();
    grpc:Error? x;

    if (create_usersStreamingClient is error) {
    } else {

        foreach User c in users {
            x = create_usersStreamingClient->sendUser(c);
        }
        grpc:Error? y = create_usersStreamingClient->complete();
        result|grpc:Error? create_usersResponse = create_usersStreamingClient->receiveResult();

        if (create_usersResponse is result) {
            io:println(create_usersResponse.message);
            io:println();
            io:println();
            io:println();
        }
        if (x is error) {
            io:println(x.cause());
        }
        if (y is error) {
            io:println(y.cause());
        }
    }
}

// where an administrator assigns each created course to an assessor;
function AssignCourses() {

    Courses[0].Assessor_Code = Assessor_usersRequest2.User_Index - 1; //DSA => Jose
    Courses[1].Assessor_Code = Assessor_usersRequest2.User_Index - 1; //AIR => Jose
    Courses[2].Assessor_Code = Assessor_usersRequest.User_Index - 1; //MIT => Kapuire
    Courses[3].Assessor_Code = Assessor_usersRequest2.User_Index - 1; //OOP => Jose
    Courses[4].Assessor_Code = Assessor_usersRequest.User_Index - 1; //PRG => Kapuire

    grpc:Error? x;

    Assign_coursesStreamingClient|grpc:Error assign_coursesStreamingClient = ep->assign_courses();

    if assign_coursesStreamingClient is error {
        var c = error:cause(assign_coursesStreamingClient);
        io:print(c);
    } else {

        foreach Course item in Courses {
            x = assign_coursesStreamingClient->sendCourse(item);
        }

        grpc:Error? y = assign_coursesStreamingClient->complete();

        result|grpc:Error? assign_coursesResponse = assign_coursesStreamingClient->receiveResult();

        if (assign_coursesResponse is result) {
            io:println(assign_coursesResponse.message);
            io:println();
            io:println();
            io:println();
        }
        if (x is error) {
            io:println(x.cause());
        }
        if (y is error) {
            io:println(y.cause());
        }
    }
}

function SubmitAssignments() {
    // SubmittedAssignments 

    grpc:Error? x;

    Submit_assignmentsStreamingClient|grpc:Error Submit_assignmentsStreamingClient = ep->submit_assignments();

    if Submit_assignmentsStreamingClient is error {
        var c = error:cause(Submit_assignmentsStreamingClient);
        io:print(c);
    } else {

        foreach Course_Assignment item in SubmittedAssignments {
            x = Submit_assignmentsStreamingClient->sendCourse_Assignment(item);
        }

        grpc:Error? y = Submit_assignmentsStreamingClient->complete();

        result|grpc:Error? Submit_assignmentsResponse = Submit_assignmentsStreamingClient->receiveResult();

        if (Submit_assignmentsResponse is result) {
            io:println(Submit_assignmentsResponse.message);
            io:println();
            io:println();
            io:println();
        }
        if (x is error) {
            io:println(x.cause());
        }
        if (y is error) {
            io:println(y.cause());
        }
    }
}

function RequestAssignments() {

    //Jose Request His Assignments 
    // AssorCode = 1001;

    result request_assignmentsRequest = {code: "1001", message: "Jose Quenuem"};
            io:println("Collecting all Assignments "); 

    stream<Course_Assignment, error?>|grpc:Error? request_assignmentsResponse = ep->request_assignments(request_assignmentsRequest);
    if request_assignmentsResponse is stream<Course_Assignment, error?> {

       var x = request_assignmentsResponse.forEach(function(Course_Assignment value) {
            io:println(`Student Number: ${value.User_Code} `); 
            io:println(`Assignment Name: ${value.Assignment_Name} `); 
            io:println(`Assignment wieght: ${value.Assignment_weight}% `); 
            io:println(`Graded: ${value.hasBeenMarked} `); 
            io:println(); 
        });
        io:print(x);

    } else if request_assignmentsResponse is grpc:Error {
        io:print("GRPC error [!]");
        
    } else {
        io:print("Unkown error [!]");
    }
 

}










const string DSA_COURSEMANAGMENTSYSTEM_DESC = "0A1F4453415F436F757273654D616E61676D656E7453797374656D2E70726F746F1A1E676F6F676C652F70726F746F6275662F77726170706572732E70726F746F2295010A0455736572121D0A0A557365725F496E646578180120012805520955736572496E64657812170A07557365725F49641802200128095206557365724964121B0A09557365725F5479706518032001280952085573657254797065121B0A09557365725F436F6465180420012809520855736572436F6465121B0A09557365725F4E616D651805200128095208557365724E616D6522E5010A11436F757273655F41737369676E6D656E7412230A0D41737369676E6D656E745F4964180120012805520C41737369676E6D656E74496412270A0F41737369676E6D656E745F4E616D65180220012809520E41737369676E6D656E744E616D6512240A0D6861734265656E4D61726B6564180320012808520D6861734265656E4D61726B656412120A044D61726B18042001280152044D61726B122B0A1141737369676E6D656E745F776569676874180520012805521041737369676E6D656E74576569676874121B0A09557365725F436F6465180620012809520855736572436F646522AD010A06436F75727365121B0A09436F757273655F49641801200128055208436F757273654964121F0A0B436F757273655F4E616D65180220012809520A436F757273654E616D6512400A124C6973745F4F665F41737365736D656E747318032003280B32122E436F757273655F41737369676E6D656E7452104C6973744F6641737365736D656E747312230A0D4173736573736F725F436F6465180420012805520C4173736573736F72436F646522360A06726573756C7412120A04636F64651801200128095204636F646512180A076D65737361676518022001280952076D65737361676532BA020A03434D5312260A0E6372656174655F636F757273657312072E436F757273651A072E726573756C742801300112240A0E61737369676E5F636F757273657312072E436F757273651A072E726573756C74280112200A0C6372656174655F757365727312052E557365721A072E726573756C74280112330A127375626D69745F61737369676E6D656E747312122E436F757273655F41737369676E6D656E741A072E726573756C74280112340A13726571756573745F61737369676E6D656E747312072E726573756C741A122E436F757273655F41737369676E6D656E743001122D0A0C7375626D69745F6D61726B7312122E436F757273655F41737369676E6D656E741A072E726573756C74280112290A08726567697374657212122E436F757273655F41737369676E6D656E741A072E726573756C742801620670726F746F33";

public isolated client class CMSClient {
    *grpc:AbstractClientEndpoint;

    private final grpc:Client grpcClient;

    public isolated function init(string url, *grpc:ClientConfiguration config) returns grpc:Error? {
        self.grpcClient = check new (url, config);
        check self.grpcClient.initStub(self, DSA_COURSEMANAGMENTSYSTEM_DESC);
    }

    isolated remote function assign_courses() returns Assign_coursesStreamingClient|grpc:Error {
        grpc:StreamingClient sClient = check self.grpcClient->executeClientStreaming("CMS/assign_courses");
        return new Assign_coursesStreamingClient(sClient);
    }

    isolated remote function create_users() returns Create_usersStreamingClient|grpc:Error {
        grpc:StreamingClient sClient = check self.grpcClient->executeClientStreaming("CMS/create_users");
        return new Create_usersStreamingClient(sClient);
    }

    isolated remote function submit_assignments() returns Submit_assignmentsStreamingClient|grpc:Error {
        grpc:StreamingClient sClient = check self.grpcClient->executeClientStreaming("CMS/submit_assignments");
        return new Submit_assignmentsStreamingClient(sClient);
    }

    isolated remote function submit_marks() returns Submit_marksStreamingClient|grpc:Error {
        grpc:StreamingClient sClient = check self.grpcClient->executeClientStreaming("CMS/submit_marks");
        return new Submit_marksStreamingClient(sClient);
    }

    isolated remote function register() returns RegisterStreamingClient|grpc:Error {
        grpc:StreamingClient sClient = check self.grpcClient->executeClientStreaming("CMS/register");
        return new RegisterStreamingClient(sClient);
    }

    isolated remote function request_assignments(result|ContextResult req) returns stream<Course_Assignment, grpc:Error?>|grpc:Error {
        map<string|string[]> headers = {};
        result message;
        if req is ContextResult {
            message = req.content;
            headers = req.headers;
        } else {
            message = req;
        }
        var payload = check self.grpcClient->executeServerStreaming("CMS/request_assignments", message, headers);
        [stream<anydata, grpc:Error?>, map<string|string[]>] [result, _] = payload;
        Course_AssignmentStream outputStream = new Course_AssignmentStream(result);
        return new stream<Course_Assignment, grpc:Error?>(outputStream);
    }

    isolated remote function request_assignmentsContext(result|ContextResult req) returns ContextCourse_AssignmentStream|grpc:Error {
        map<string|string[]> headers = {};
        result message;
        if req is ContextResult {
            message = req.content;
            headers = req.headers;
        } else {
            message = req;
        }
        var payload = check self.grpcClient->executeServerStreaming("CMS/request_assignments", message, headers);
        [stream<anydata, grpc:Error?>, map<string|string[]>] [result, respHeaders] = payload;
        Course_AssignmentStream outputStream = new Course_AssignmentStream(result);
        return {content: new stream<Course_Assignment, grpc:Error?>(outputStream), headers: respHeaders};
    }

    isolated remote function create_courses() returns Create_coursesStreamingClient|grpc:Error {
        grpc:StreamingClient sClient = check self.grpcClient->executeBidirectionalStreaming("CMS/create_courses");
        return new Create_coursesStreamingClient(sClient);
    }
}

public client class Assign_coursesStreamingClient {
    private grpc:StreamingClient sClient;

    isolated function init(grpc:StreamingClient sClient) {
        self.sClient = sClient;
    }

    isolated remote function sendCourse(Course message) returns grpc:Error? {
        return self.sClient->send(message);
    }

    isolated remote function sendContextCourse(ContextCourse message) returns grpc:Error? {
        return self.sClient->send(message);
    }

    isolated remote function receiveResult() returns result|grpc:Error? {
        var response = check self.sClient->receive();
        if response is () {
            return response;
        } else {
            [anydata, map<string|string[]>] [payload, _] = response;
            return <result>payload;
        }
    }

    isolated remote function receiveContextResult() returns ContextResult|grpc:Error? {
        var response = check self.sClient->receive();
        if response is () {
            return response;
        } else {
            [anydata, map<string|string[]>] [payload, headers] = response;
            return {content: <result>payload, headers: headers};
        }
    }

    isolated remote function sendError(grpc:Error response) returns grpc:Error? {
        return self.sClient->sendError(response);
    }

    isolated remote function complete() returns grpc:Error? {
        return self.sClient->complete();
    }
}

public client class Create_usersStreamingClient {
    private grpc:StreamingClient sClient;

    isolated function init(grpc:StreamingClient sClient) {
        self.sClient = sClient;
    }

    isolated remote function sendUser(User message) returns grpc:Error? {
        return self.sClient->send(message);
    }

    isolated remote function sendContextUser(ContextUser message) returns grpc:Error? {
        return self.sClient->send(message);
    }

    isolated remote function receiveResult() returns result|grpc:Error? {
        var response = check self.sClient->receive();
        if response is () {
            return response;
        } else {
            [anydata, map<string|string[]>] [payload, _] = response;
            return <result>payload;
        }
    }

    isolated remote function receiveContextResult() returns ContextResult|grpc:Error? {
        var response = check self.sClient->receive();
        if response is () {
            return response;
        } else {
            [anydata, map<string|string[]>] [payload, headers] = response;
            return {content: <result>payload, headers: headers};
        }
    }

    isolated remote function sendError(grpc:Error response) returns grpc:Error? {
        return self.sClient->sendError(response);
    }

    isolated remote function complete() returns grpc:Error? {
        return self.sClient->complete();
    }
}

public client class Submit_assignmentsStreamingClient {
    private grpc:StreamingClient sClient;

    isolated function init(grpc:StreamingClient sClient) {
        self.sClient = sClient;
    }

    isolated remote function sendCourse_Assignment(Course_Assignment message) returns grpc:Error? {
        return self.sClient->send(message);
    }

    isolated remote function sendContextCourse_Assignment(ContextCourse_Assignment message) returns grpc:Error? {
        return self.sClient->send(message);
    }

    isolated remote function receiveResult() returns result|grpc:Error? {
        var response = check self.sClient->receive();
        if response is () {
            return response;
        } else {
            [anydata, map<string|string[]>] [payload, _] = response;
            return <result>payload;
        }
    }

    isolated remote function receiveContextResult() returns ContextResult|grpc:Error? {
        var response = check self.sClient->receive();
        if response is () {
            return response;
        } else {
            [anydata, map<string|string[]>] [payload, headers] = response;
            return {content: <result>payload, headers: headers};
        }
    }

    isolated remote function sendError(grpc:Error response) returns grpc:Error? {
        return self.sClient->sendError(response);
    }

    isolated remote function complete() returns grpc:Error? {
        return self.sClient->complete();
    }
}

public client class Submit_marksStreamingClient {
    private grpc:StreamingClient sClient;

    isolated function init(grpc:StreamingClient sClient) {
        self.sClient = sClient;
    }

    isolated remote function sendCourse_Assignment(Course_Assignment message) returns grpc:Error? {
        return self.sClient->send(message);
    }

    isolated remote function sendContextCourse_Assignment(ContextCourse_Assignment message) returns grpc:Error? {
        return self.sClient->send(message);
    }

    isolated remote function receiveResult() returns result|grpc:Error? {
        var response = check self.sClient->receive();
        if response is () {
            return response;
        } else {
            [anydata, map<string|string[]>] [payload, _] = response;
            return <result>payload;
        }
    }

    isolated remote function receiveContextResult() returns ContextResult|grpc:Error? {
        var response = check self.sClient->receive();
        if response is () {
            return response;
        } else {
            [anydata, map<string|string[]>] [payload, headers] = response;
            return {content: <result>payload, headers: headers};
        }
    }

    isolated remote function sendError(grpc:Error response) returns grpc:Error? {
        return self.sClient->sendError(response);
    }

    isolated remote function complete() returns grpc:Error? {
        return self.sClient->complete();
    }
}

public client class RegisterStreamingClient {
    private grpc:StreamingClient sClient;

    isolated function init(grpc:StreamingClient sClient) {
        self.sClient = sClient;
    }

    isolated remote function sendCourse_Assignment(Course_Assignment message) returns grpc:Error? {
        return self.sClient->send(message);
    }

    isolated remote function sendContextCourse_Assignment(ContextCourse_Assignment message) returns grpc:Error? {
        return self.sClient->send(message);
    }

    isolated remote function receiveResult() returns result|grpc:Error? {
        var response = check self.sClient->receive();
        if response is () {
            return response;
        } else {
            [anydata, map<string|string[]>] [payload, _] = response;
            return <result>payload;
        }
    }

    isolated remote function receiveContextResult() returns ContextResult|grpc:Error? {
        var response = check self.sClient->receive();
        if response is () {
            return response;
        } else {
            [anydata, map<string|string[]>] [payload, headers] = response;
            return {content: <result>payload, headers: headers};
        }
    }

    isolated remote function sendError(grpc:Error response) returns grpc:Error? {
        return self.sClient->sendError(response);
    }

    isolated remote function complete() returns grpc:Error? {
        return self.sClient->complete();
    }
}

public class Course_AssignmentStream {
    private stream<anydata, grpc:Error?> anydataStream;

    public isolated function init(stream<anydata, grpc:Error?> anydataStream) {
        self.anydataStream = anydataStream;
    }

    public isolated function next() returns record {|Course_Assignment value;|}|grpc:Error? {
        var streamValue = self.anydataStream.next();
        if (streamValue is ()) {
            return streamValue;
        } else if (streamValue is grpc:Error) {
            return streamValue;
        } else {
            record {|Course_Assignment value;|} nextRecord = {value: <Course_Assignment>streamValue.value};
            return nextRecord;
        }
    }

    public isolated function close() returns grpc:Error? {
        return self.anydataStream.close();
    }
}

public client class Create_coursesStreamingClient {
    private grpc:StreamingClient sClient;

    isolated function init(grpc:StreamingClient sClient) {
        self.sClient = sClient;
    }

    isolated remote function sendCourse(Course message) returns grpc:Error? {
        return self.sClient->send(message);
    }

    isolated remote function sendContextCourse(ContextCourse message) returns grpc:Error? {
        return self.sClient->send(message);
    }

    isolated remote function receiveResult() returns result|grpc:Error? {
        var response = check self.sClient->receive();
        if response is () {
            return response;
        } else {
            [anydata, map<string|string[]>] [payload, _] = response;
            return <result>payload;
        }
    }

    isolated remote function receiveContextResult() returns ContextResult|grpc:Error? {
        var response = check self.sClient->receive();
        if response is () {
            return response;
        } else {
            [anydata, map<string|string[]>] [payload, headers] = response;
            return {content: <result>payload, headers: headers};
        }
    }

    isolated remote function sendError(grpc:Error response) returns grpc:Error? {
        return self.sClient->sendError(response);
    }

    isolated remote function complete() returns grpc:Error? {
        return self.sClient->complete();
    }
}

public type ContextResultStream record {|
    stream<result, error?> content;
    map<string|string[]> headers;
|};

public type ContextUserStream record {|
    stream<User, error?> content;
    map<string|string[]> headers;
|};

public type ContextCourseStream record {|
    stream<Course, error?> content;
    map<string|string[]> headers;
|};

public type ContextCourse_AssignmentStream record {|
    stream<Course_Assignment, error?> content;
    map<string|string[]> headers;
|};

public type ContextResult record {|
    result content;
    map<string|string[]> headers;
|};

public type ContextUser record {|
    User content;
    map<string|string[]> headers;
|};

public type ContextCourse record {|
    Course content;
    map<string|string[]> headers;
|};

public type ContextCourse_Assignment record {|
    Course_Assignment content;
    map<string|string[]> headers;
|};

@protobuf:Descriptor {value: DSA_COURSEMANAGMENTSYSTEM_DESC}
public type result record {|
    string code = "";
    string message = "";
|};

@protobuf:Descriptor {value: DSA_COURSEMANAGMENTSYSTEM_DESC}
public type User record {|
    int User_Index = 0;
    string User_Id = "";
    string User_Type = "";
    string User_Code = "";
    string User_Name = "";
|};

@protobuf:Descriptor {value: DSA_COURSEMANAGMENTSYSTEM_DESC}
public type Course_Assignment record {|
    int Assignment_Id = 0;
    string Assignment_Name = "";
    boolean hasBeenMarked = false;
    float Mark = 0.0;
    int Assignment_weight = 0;
    string User_Code = "";
|};

@protobuf:Descriptor {value: DSA_COURSEMANAGMENTSYSTEM_DESC}
public type Course record {|
    int Course_Id = 0;
    string Course_Name = "";
    Course_Assignment[] List_Of_Assesments = [];
    int Assessor_Code = 0;
|};

